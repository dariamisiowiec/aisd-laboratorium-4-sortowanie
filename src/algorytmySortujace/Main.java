package algorytmySortujace;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int RANGE = 100000;
        FileHandler fileHandler = new FileHandler();
        Randomize randomize = new Randomize();
        QuickSort quickSort = new QuickSort();
        MergeSort mergeSort = new MergeSort();
        ShellSort shellSort = new ShellSort();

        long startTime, sortingTime;
        List<Integer> howManyNumbers =
                fileHandler.getNumbersFromFile("C:\\Users\\Anna1\\Projekty\\AiSDLab4Sortowania\\input.txt");

   /*     File quickSortNOO = new File("quickSortNOO.txt");
        File mergeSortNOO = new File("mergeSortNOO.txt");
        File shell2SortNOO = new File("shell2SortNOO.txt");
        File shell3SortNOO = new File("Shell3SortNOO.txt"); */

        for (int i = 0; i < howManyNumbers.size(); i++) {
         int[] unsortedArray;
         unsortedArray = randomize.getArrayWithRandomNumbersInRange(howManyNumbers.get(i), RANGE);
         String fileName = "unsorted" + howManyNumbers.get(i) + ".txt";
         fileHandler.writeFromArrayToFile(unsortedArray, fileName);

         // QuickSort

         quickSort.numberOfOperations = 0;
         int[] arrayForQuickSort = Arrays.copyOf(unsortedArray, unsortedArray.length);
         startTime = System.currentTimeMillis();
         quickSort.quickSort(arrayForQuickSort, 0, unsortedArray.length - 1);
         sortingTime = System.currentTimeMillis() - startTime;
         System.out.println("QuickSort for " + howManyNumbers.get(i) + " elements: " + sortingTime + " miliseconds"
                            + "; number of operations = " + quickSort.numberOfOperations);

         fileName = "quickSort" + howManyNumbers.get(i) + ".txt";
         fileHandler.writeFromArrayToFile(arrayForQuickSort, fileName);
     //    fileHandler.addNumberToFile(quickSort.numberOfOperations, "quickSortNOO.txt");



            // MergeSort

            mergeSort.numberOfOperations = 0;
            int[] arrayForMergeSort = Arrays.copyOf(unsortedArray, unsortedArray.length);
            startTime = System.currentTimeMillis();
            mergeSort.mergeSort(arrayForMergeSort);
            sortingTime = System.currentTimeMillis() - startTime;
            System.out.println("MergeSort for " + howManyNumbers.get(i) + " elements: " + sortingTime + " miliseconds" +
                               "; number of operations = " + mergeSort.numberOfOperations);

            fileName = "mergeSort" + howManyNumbers.get(i) + ".txt";
            fileHandler.writeFromArrayToFile(arrayForMergeSort, fileName);
      //      fileHandler.addNumberToFile(mergeSort.numberOfOperations, "mergeSortNOO.txt");

            // ShellSort

            shellSort.numberOfOperations = 0;
            int[] arrayForShellSort_2 = Arrays.copyOf(unsortedArray, unsortedArray.length);
            startTime = System.currentTimeMillis();
            shellSort.shellSort(arrayForShellSort_2, 2);
            sortingTime = System.currentTimeMillis() - startTime;
            System.out.println("ShellSort_2 for " + howManyNumbers.get(i) + " elements: " + sortingTime + " miliseconds"
                                + "; number of operations = " + shellSort.numberOfOperations);


            fileName = "shellSort_2_" + howManyNumbers.get(i) + ".txt";
            fileHandler.writeFromArrayToFile(arrayForMergeSort, fileName);
      //      fileHandler.addNumberToFile(shellSort.numberOfOperations, "shell2SortNOO.txt");


            shellSort.numberOfOperations = 0;
            int[] arrayForShellSort_3 = Arrays.copyOf(unsortedArray, unsortedArray.length);
            startTime = System.currentTimeMillis();
            shellSort.shellSort(arrayForShellSort_3, 3);
            sortingTime = System.currentTimeMillis() - startTime;
            System.out.println("ShellSort_3 for " + howManyNumbers.get(i) + " elements: " + sortingTime + " miliseconds"
                                + "; number of operations = " + shellSort.numberOfOperations);

            fileName = "shellSort_3_" + howManyNumbers.get(i) + ".txt";
            fileHandler.writeFromArrayToFile(arrayForMergeSort, fileName);
      //      fileHandler.addNumberToFile(shellSort.numberOfOperations, "shell3SortNOO.txt");


        }

    }
}
