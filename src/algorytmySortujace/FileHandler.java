package algorytmySortujace;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileHandler {

    private Scanner scanner;
    private PrintWriter writer;

    public List<Integer> getNumbersFromFile (String fileName) {
        List<Integer> integerList = new LinkedList<>();
        try {
            scanner = new Scanner(new File(fileName));
            while (scanner.hasNextInt()) {
                integerList.add(scanner.nextInt());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return integerList;
    }

    public void writeFromArrayToFile (int[] array, String fileName) {
        try {
            writer = new PrintWriter(new FileWriter(fileName, true));
            for (int i = 0; i < array.length; i++) {
                writer.print(array[i] + " ");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void addNumberToFile (int number, String fileName) {
        try {
            writer = new PrintWriter(new FileWriter(fileName, true));
                writer.print(number + " ");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
