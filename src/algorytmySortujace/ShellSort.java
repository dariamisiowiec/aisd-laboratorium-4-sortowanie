package algorytmySortujace;

public class ShellSort {
    int numberOfOperations = 0;

    public void shellSort(int[] tab, int divideBy) {
        int numberOfelements = tab.length;
        int gap = numberOfelements/divideBy;
        int firstIndex;
        int endIndex;
        int temp;
        while (gap>=1){
            firstIndex = 0;
            endIndex = gap;
            while (endIndex<numberOfelements) {
                int i = firstIndex;
                int j = endIndex;
                while (i>=0 && tab[i] > tab[j]) {
                    temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;
                    i = i - gap;
                    j = j - gap;
                    numberOfOperations++;
                }
                endIndex += 1;
                firstIndex += 1;
            }
            gap = gap/2;
        }
    }




}
